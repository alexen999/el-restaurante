<?php

if (!isset($_POST['username']) || !isset($_POST['password'])){
    echo json_encode(array('code'=>-1, 'msg'=>'Acceso denegado'));
    exit();
}
if (empty($_POST['username']) || empty($_POST['password'])){
    echo json_encode(array('code'=>-2, 'msg'=>'Rellene todos los campos'));
    exit();
}
if ($_POST['username'] != 'tapstation' && $_POST['password'] != 'tapstation2020'){
    echo json_encode(array('code'=>-3, 'msg'=>'Error de autenticación'));
    exit();
}

echo json_encode(array('code'=>1, 'msg'=>'Acceso correcto'));
?>