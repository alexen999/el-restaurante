<?php
if (!isset($_POST['tipo_menu'])){
    echo '<script>parent.$(".container-menu .info-login").html("Error de servidor");</script>';
    exit();
}
if (!isset($_FILES['fichero'])){
    echo '<script>parent.$(".container-menu .info-login").html("Fichero no encontrado");</script>';
    exit();
}
switch(intval($_POST['tipo_menu'])){
    case 0: $menu = 'menu-desayuno.pdf'; break;
    case 1: $menu = 'menu-diario.pdf'; break;
    case 2: $menu = 'menu-navidad.pdf'; break;
    case 3: $menu = 'menu-especial.pdf'; break;
}

if (move_uploaded_file($_FILES["fichero"]["tmp_name"], '../docs/'.$menu)) {
    echo "<script>parent.location.href='/eden';</script>";
} else {
    echo '<script>parent.$(".container-menu .info-login").html("Error al cargar el fichero");</script>';
    exit();
}
?>