<?php

$diario = $navidad = $especial = false;
$cols = 0;

if (file_exists('../docs/menu-diario.pdf')){
    $diario = true;
    $cols += 1;
}
if (file_exists('../docs/menu-navidad.pdf')){
    $navidad = true;
    $cols += 1;
}
if (file_exists('../docs/menu-especial.pdf')){
    $especial = true;
    $cols += 1;
}

echo json_encode(array('diario'=>$diario, 'navidad'=>$navidad, 'especial'=>$especial, 'cols'=>$cols));

?>